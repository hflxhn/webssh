#!/bin/bash
#2020年 05月 06日 星期三 10:34:29 CST

# start webssh
function webssh() {
    $1 $2 &
}

function isRuning() {
    num=`ps -ef | grep python | grep $2 | wc -l`

    if [[ $num == 0 ]]; then
        webssh $1 $2
        echo error
    fi
}

isRuning /usr/bin/python /var/www/webssh/main.py
